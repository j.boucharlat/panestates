jQuery(document).ready(function($) {

    $('.acfgs-slider').each(function(index) {

        var sconf = {};
        var slider_id = $(this).attr('id');
        var slider_conf = $.parseJSON($(this).parent('.acfgs-slider-wrap').find('.acfgs-slider-conf').text());
        // console.log(slider_conf);

        jQuery('#' + slider_id + ' .acfgs-gallery-slider').slick({
            // values from php template
            asNavFor: slider_conf.asNavFor,
            dots: (slider_conf.dots) == "true" ? true : false,
            arrows: (slider_conf.arrows) == "true" ? true : false,
            speed: parseInt(slider_conf.speed),
            autoplay: (slider_conf.autoplay) == "true" ? true : false,
            autoplaySpeed: parseInt(slider_conf.autoplay_speed),
            rtl: (slider_conf.rtl) == "true" ? true : false,
            // local values
            slidesToShow: 1,
            slidesToScroll: 1,
            fade: true,
        });
        // jQuery('#'+slider_id+' .acfgs-gallery-slider').slick();

    });

    $('.acfgs-slider-strip').each(function(index) {

        var sconf = {};
        var slider_id = $(this).attr('id');
        var slider_conf = $.parseJSON($(this).parent('.acfgs-slider-wrap').find('.acfgs-filmstrip-conf').text());
        // console.log(slider_conf);

        jQuery('#' + slider_id + ' .acfgs-gallery-slider').slick({
            // values from php template
            asNavFor: slider_conf.asNavFor,
            rtl: (slider_conf.rtl) == "true" ? true : false,
            // local values
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
            infinite: true,
            arrows: false,
            centerMode: true,
            focusOnSelect: true,
        });
        // jQuery('#'+slider_id+' .acfgs-gallery-slider').slick();
    });

    $('.acfgs-carousel').each(function(index) {

        var sconf = {};
        var slider_id = $(this).attr('id');
        var slider_conf = $.parseJSON($(this).parent('.acfgs-carousel-wrap').find('.acfgs-carousel-conf').text());

        jQuery('#' + slider_id + ' .acfgs-gallery-carousel').slick({
            dots: (slider_conf.dots) == "true" ? true : false,
            // infinite        : true,
            arrows: (slider_conf.arrows) == "true" ? true : false,
            speed: parseInt(slider_conf.speed),
            autoplay: (slider_conf.autoplay) == "true" ? true : false,
            autoplaySpeed: parseInt(slider_conf.autoplay_speed),
            slidesToShow: parseInt(slider_conf.slide_to_show),
            slidesToScroll: parseInt(slider_conf.slide_to_scroll),
            rtl: (slider_conf.rtl) == "true" ? true : false,
            // adaptiveHeight  : true,
            mobileFirst: (Fagsfacf.is_mobile == 1) ? true : false,
            responsive: [{
                    breakpoint: 1023,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: false
                    }
                }, {

                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 479,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: false
                    }
                },
                {
                    breakpoint: 319,
                    settings: {
                        slidesToShow: 1,
                        slidesToScstrip: 1,
                        dots: false
                    }
                }
            ]
        });
    });

});

<?php
/**
 *  shortcode [acf_gallery_carousel]
 *
 */

add_shortcode('acf_gallery_carousel', 'acfgs_carousel');
function acfgs_carousel($atts){

    extract(shortcode_atts(array(
        'acf_field'        => '',
        'slide_to_show'    => '2',
        'slide_to_scroll'  => '1',
        'autoplay'         => 'true',
        'autoplay_speed'   => '3000',
        'speed'            => '300',
        'arrows'           => 'true',
        'dots'             => 'true',
        'show_caption'     => 'true',
        'rtl'              => '',

    ), $atts));

    if ($show_caption) {
        $show_caption_value = $show_caption;
    } else {
        $show_caption_value = 'false';
    }

    if ($acf_field) {
        $acf_field_value = $acf_field;
    } else {
        $acf_field_value = '';
    }


    $slide_to_show      = !empty($slide_to_show)    ? $slide_to_show    : 3;
    $slide_to_scroll    = !empty($slide_to_scroll)  ? $slide_to_scroll  : 1;
    $dots               = ($dots == 'false')        ? 'false'           : 'true';
    $arrows             = ($arrows == 'false')      ? 'false'           : 'true';
    $autoplay           = ($autoplay == 'false')    ? 'false'           : 'true';
    $autoplay_speed     = (!empty($autoplay_speed)) ? $autoplay_speed   : 3000;
    $speed              = (!empty($speed))          ? $speed            : 300;


    // For RTL
    if (empty($rtl) && is_rtl()) {
        $rtl = 'true';
    } elseif ($rtl == 'true') {
        $rtl = 'true';
    } else {
        $rtl = 'false';
    }

    $unique = acfgs_get_unique();
    wp_enqueue_script('slick-jquery');
    wp_enqueue_script('acfgs-public-script');
    // carousel configuration
    $slider_conf = compact('slide_to_show', 'slide_to_scroll', 'autoplay', 'autoplay_speed', 'speed', 'arrows', 'dots', 'rtl');
    ob_start();
    // $images = get_field($acf_field_value);
    $images = acf_photo_gallery('pictures_gallery', get_the_ID());

    if ($images): ?>
        <div class="acfgs-carousel-wrap">
          <div id="acfgs-carousel-<?php echo $unique; ?>" class="acfgs-carousel">
            <div class="acfgs-gallery-carousel">
              <?php foreach ($images as $image): ?>
                <div class="acfgs-gallery-slide">
                  <div class="acfgs-gallery-slide-inner">
                    <img src="<?php echo $image['full_image_url']; ?>" alt="<?php echo $image['title']; ?>" />
                    <?php if ($show_caption_value == 'true') { ?>
                      <div class="acfgs-gallery-caption"><?php echo $image['caption']; ?></div>
                    <?php } ?>
                  </div>
                </div>
              <?php endforeach; ?>
            </div>
            <div class="acfgs-carousel-conf"><?php echo json_encode($slider_conf); ?></div><!-- end of-slider-conf -->
          </div>
        </div>
    <?php endif;
    return ob_get_clean();
}

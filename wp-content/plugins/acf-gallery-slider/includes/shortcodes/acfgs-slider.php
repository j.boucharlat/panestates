<?php
/**
 * shortcode [acf_gallery_slider]
 *
 */

add_shortcode('acf_gallery_slider', 'acfgs_slider');
function acfgs_slider($atts)
{
    /**
     * Getting shortcode settings & setting some defaults
     * Defaults are also set in plugin's js initialisation : acfgs-public.js
     */
    // echo "<pre>";
    // print_r($atts);
    // echo "</pre></br></br>";
    $atts = shortcode_atts(array(
        'acf_field'         => '',
        'autoplay'          => 'true',
        'autoplay_speed'    => '3000',
        'speed'             => '500',
        'arrows'            => 'true',
        'dots'              => 'true',
        'show_caption'      => '',
        'rtl'               => '',
        'filmstrip'          => 'false'
    ), $atts);


    // echo "acf_field " . $acf_field . "<br>";
    // echo "autoplay " . $autoplay . "<br>";
    // echo "autoplay_speed " . $autoplay_speed . "<br>";
    // echo "speed " . $speed . "<br>";
    // echo "arrows " . $arrows . "<br>";
    // echo "dots " . $dots . "<br>";
    // echo "show_caption " . $show_caption . "<br>";
    // echo "rtl " . $rtl . "<br>";
    // echo "filmstrip " . $filmstrip . "<br><br>";

    extract($atts);

    // For RTL
    if (empty($rtl) && is_rtl()) {
        $rtl = 'true';
    } elseif ($rtl == 'true') {
        $rtl = 'true';
    } else {
        $rtl = 'false';
    }

    // For slider ID
    $unique = acfgs_get_unique();
    // Slider configuration
    if ($filmstrip == 'true') {
        $asNavFor = '#acfgs-slider-strip-' . $unique . ' .acfgs-gallery-slider';
    }
    $slider_conf = compact('autoplay', 'autoplay_speed', 'speed', 'arrows', 'dots', 'rtl', 'asNavFor');

    // For filmstrip
    if ($filmstrip == 'true') {
        $asNavFor = '#acfgs-slider-' . $unique . ' .acfgs-gallery-slider';
        $arrows = 'false';
        $dots = 'false';
        // Filmstrip configuration // TODO: remove / cleanup settings
        // $filmstrip_conf = compact('autoplay', 'autoplay_speed', 'speed', 'arrows', 'dots', 'rtl', 'asNavFor');
        $filmstrip_conf = compact('arrows', 'dots', 'rtl', 'asNavFor');
    }

    // Loading JS
    wp_enqueue_script('slick-jquery');
    wp_enqueue_script('acfgs-public-script');

    ob_start();
    // $images = get_field($acf_field_value);
    $images = acf_photo_gallery( $acf_field, get_the_ID());

    if ($images): ?>
        <div class="acfgs-slider-wrap">
          <div id="acfgs-slider-<?php echo $unique; ?>" class="acfgs-slider">
            <div class="acfgs-gallery-slider" data-slick='<?php //echo json_encode($slider_conf); // TODO: reconciliate PHP & JS var names to use data-slick?>' >
              <?php foreach ($images as $image): ?>
                <div class="acfgs-gallery-slide">
                  <div class="acfgs-gallery-slide-inner">
                    <img src="<?php echo $image['full_image_url']; ?>" alt="<?php echo $image['title']; ?>" />
                    <?php if ($show_caption == 'true') { ?>
                      <div class="acfgs-gallery-caption"><?php echo $image['caption']; ?></div>
                    <?php } ?>
                  </div>
                </div>
              <?php endforeach; ?>
            </div>
            <div class="acfgs-slider-conf"><?php echo json_encode($slider_conf); ?></div>
            <!-- end of-slider-conf -->
          </div>
          <?php if ( $atts['filmstrip'] == 'true' ) { ?>
              <div id="acfgs-slider-strip-<?php echo $unique; ?>" class="acfgs-slider-strip">
                  <div class="acfgs-gallery-slider" data-slick='<?php //echo json_encode($filmstrip_conf); // TODO: reconciliate PHP & JS var names to use data-slick?>'>
                      <?php foreach ($images as $image): ?>
                        <div class="acfgs-gallery-slide-thumb">
                          <div class="acfgs-gallery-slide-inner">
                            <img src="<?php echo $image['full_image_url']; ?>" alt="<?php echo $image['title']; ?>" />
                          </div>
                        </div>
                      <?php endforeach; ?>
                      <?php // Needs to repeat markup to avoid "holes" in the strip
                      foreach ($images as $image): ?>
                        <div class="acfgs-gallery-slide-thumb">
                          <div class="acfgs-gallery-slide-inner">
                            <img src="<?php echo $image['full_image_url']; ?>" alt="<?php echo $image['title']; ?>" />
                          </div>
                        </div>
                      <?php endforeach; ?>
                </div>
                <div class="acfgs-filmstrip-conf"><?php echo json_encode($filmstrip_conf); ?></div>
                <!-- end of-slider-conf -->
              </div>
          <?php } ?>
        </div>
        <?php endif;

    return ob_get_clean();
}

<?php
/**
 * Script Class
 *
 * Handles the script and style functionality of plugin
 *
 * @package ACF Gallery Slim Slider
 * @since 1.0.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

class Fagsfacf_Script {
    
    function __construct() {
        
        // Action to add style at front side
        add_action( 'wp_enqueue_scripts', array($this, 'acfgs_front_style') );
        
        // Action to add script at front side
        add_action( 'wp_enqueue_scripts', array($this, 'acfgs_front_script') );    
        
    }

    /**
     * Function to add style at front side
     * 
     * @package ACF Gallery Slim Slider
     * @since 1.0.0
     */
    function acfgs_front_style() {
        
        // Registring and enqueing slick slider css
        if( !wp_style_is( 'slick-style', 'registered' ) ) {
            wp_register_style( 'slick-style', ACFGS_VERSION_URL.'assets/css/slick.css', array(), ACFGS_VERSION );
            wp_enqueue_style( 'slick-style' );
        }
        
        // Registring and enqueing public css
        wp_register_style( 'acfgs-public-style', ACFGS_VERSION_URL.'assets/css/acfgs-public-css.css', array(), ACFGS_VERSION );
        wp_enqueue_style( 'acfgs-public-style' );
    }
    
    /**
     * Function to add script at front side
     * 
     * @package ACF Gallery Slim Slider
     * @since 1.0.0
     */
    function acfgs_front_script() {
        
        // Registring slick slider script
        if( !wp_script_is( 'slick-jquery', 'registered' ) ) {
            wp_register_script( 'slick-jquery', ACFGS_VERSION_URL.'assets/js/slick.min.js', array('jquery'), ACFGS_VERSION, true );
        }
        
        // Registring and enqueing public script
        wp_register_script( 'acfgs-public-script', ACFGS_VERSION_URL.'assets/js/acfgs-public.js', array('jquery'), ACFGS_VERSION, true );
        wp_localize_script( 'acfgs-public-script', 'Fagsfacf', array(
                                                                    'is_mobile' => (wp_is_mobile()) ? 1 : 0                                                                    
                                                                    ));
    }
    
}

$acfgs_script = new Fagsfacf_Script();
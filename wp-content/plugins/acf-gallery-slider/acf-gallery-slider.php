<?php
/**
 * Plugin Name: ACF Gallery Slim Slider
 * Description: Display ACF Photo Gallery Field on frontend of your website with shorcode.
 * Author: Julien
 * Text Domain: acf-gallery-slider
 * Domain Path: /languages/
 * Version: 1.0
 * Author URI: http://www.wponlinesupport.com/
 *
 * @package WordPress
 * @author Julien
 */

// Exit if accessed directly
if (!defined('ABSPATH')) {
    exit;
}

if (!defined('ACFGS_VERSION')) {
    define('ACFGS_VERSION', '1.0'); // Version of plugin // Usefull??
}
if (!defined('ACFGS_VERSION_DIR')) {
    define('ACFGS_VERSION_DIR', dirname(__FILE__)); // Plugin dir
}
if (!defined('ACFGS_VERSION_URL')) {
    define('ACFGS_VERSION_URL', plugin_dir_url(__FILE__)); // Plugin url
}
if (!defined('ACFGS_POST_TYPE')) {
    define('ACFGS_POST_TYPE', 'acf'); // plugin post type
}

add_action('plugins_loaded', 'acfgs_load_textdomain');
function acfgs_load_textdomain()
{
    load_plugin_textdomain('acf-gallery-slider', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

/**
 * Activation Hook
 *
 * Register plugin activation hook.
 *
 * @package ACF Gallery Slim Slider
 * @since 1.0.0
 */
register_activation_hook(__FILE__, 'acfgs_install');

/**
 * Plugin Setup (On Activation)
 *
 * Does the initial setup,
 * set default values for the plugin options.
 *
 * @package ACF Gallery Slim Slider
 * @since 1.0.0
 */
function acfgs_install()
{
    if (is_plugin_active('acf-gallery-slider/acf-gallery-slider.php')) {
        // add_action('update_option_active_plugins', 'acfgs_deactivate_pro_version');
    }
}

/**
 * Check ACF plugin is active
 * TODO adapt to Frontend Gallery Slider For ACF
 *
 * @package ACF Gallery Slim Slider
 * @since 1.0.0
 */
function acfgs_check_activation()
{
    if (!class_exists('acf')) {
        // is this plugin active?
        if (is_plugin_active(plugin_basename(__FILE__))) {
            // deactivate the plugin
            deactivate_plugins(plugin_basename(__FILE__));
            // unset activation notice
            unset($_GET[ 'activate' ]);
            // display notice
            add_action('admin_notices', 'acfgs_admin_notices');
        }
    }
}

// Check required plugin is activated or not
add_action('admin_init', 'acfgs_check_activation');

/**
 * Admin notices
 *
 * @package ACF Gallery Slim Slider
 * @since 1.0.0
 */
function acfgs_admin_notices()
{
    if (!class_exists('acf')) {
        echo '<div class="error notice is-dismissible">';
        echo sprintf(__('<p><strong>%s</strong> recommends the following plugin to use.</p>', 'acf-gallery-slider'), 'ACF Gallery Slim Slider');
        echo sprintf(__('<p><strong><a href="%s" target="_blank">%s</a>, %s</strong></p>', 'acf-gallery-slider'), 'https://wordpress.org/plugins/advanced-custom-fields', 'Advanced Custom Fields', '');// TODO: fix comma
        echo sprintf(__('<p><strong><a href="%s" target="_blank">%s</a>, %s</strong></p>', 'acf-gallery-slider'), 'https://wordpress.org/plugins/navz-photo-gallery', 'ACF Photo Gallery Field', '');// TODO: fix comma        echo '</div>';
    }
}

/**
 * Function to unique number value
 *
 */
function acfgs_get_unique()
{
    static $unique = 0;
    $unique++;

    return $unique;
}

// Script
require_once(ACFGS_VERSION_DIR . '/includes/class-acfgs-script.php');

// Shortcodes
require_once(ACFGS_VERSION_DIR . '/includes/shortcodes/acfgs-slider.php');
require_once(ACFGS_VERSION_DIR . '/includes/shortcodes/acfgs-carousel.php');

<?php
/**
 * PAN Estates theme functions
 *
 * Child theme of Hotel Luxury by FilaThemes
 * https://www.filathemes.com/downloads/hotel-luxury/
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @version 1.0.0
 *
 * @package PAN_Estates
 *
 */

// Exit if accessed directly
 if (!defined('ABSPATH')) {
     exit;
 }

/**
 * Disabeling autosave
 * https://trickspanda.com/how-to-disable-wordpress-autosave/
 * https://www.h3xed.com/web-development/wordpress-disable-autosaves-delete-all-autosaves-from-database
 *
 */
add_action( 'admin_init', 'disable_autosave' );
function disable_autosave() {
        wp_deregister_script( 'autosave' );
}

/**
 * Enqueues scripts properly. Do not remove this function.
 */
add_action('wp_enqueue_scripts', 'panestates_enqueue', 20 );
function panestates_enqueue()
{
    // Deregistering parent scripts 'wp_enqueue_scripts' NOT EFFECTIVE
    // remove_action('wp_enqueue_scripts', 'hotel_luxury_scripts', 10 );

    // START Parent scripts sequence // TODO: remove foreign code
    // from hotel-luxury/functions.php:199
    // wp_enqueue_style( 'hotel-luxury-fonts', hotel_luxury_fonts_url(), array(), null );
    // wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), '4.0.0', '' );
    // wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css', array(), '4.0', '' );
    // wp_enqueue_style( 'hotel-luxury-woocommerce-modify', get_template_directory_uri() . '/assets/css/woocommerce-modify.css', array(), '0.0.1', '' );
    // wp_enqueue_style( 'magnific-popup', get_template_directory_uri() . '/assets/css/magnific-popup.css', array(), '1.1.0', '' );
    // wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/assets/css/owl.carousel.min.css', array(), '2.2.1', '' );
    // wp_enqueue_style( 'owl-theme-default', get_template_directory_uri() . '/assets/css/owl.theme.default.min.css', array(), '2.2.1', '' );

    // Loading parent style
    wp_dequeue_style('hotel-luxury-style');
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
    // Loading child style
    wp_enqueue_style( 'child-style', get_stylesheet_uri() );

    // wp_enqueue_script( 'jquery-magnific', get_template_directory_uri() . '/assets/js/jquery.magnific-popup.min.js', array( 'jquery' ), '1.1.0', true );
    // wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array( 'jquery' ), '2.2.1', true );

    // incuded custom.js in child theme
    wp_dequeue_script( 'hotel-luxury-custom' );
    wp_enqueue_script( 'panestate-custom', get_stylesheet_directory_uri() . '/include/js/custom.js', array( 'jquery' ), '20151215', true );

    // if ( function_exists( 'hotel_luxury_custom_style' ) ) {
    //     wp_add_inline_style( 'hotel-luxury-style', hotel_luxury_custom_style() );
    // }
    //
    // if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
    //     wp_enqueue_script( 'comment-reply' );
    // }
    // END Parent scripts sequence // TODO: remove foreign code

}

/* CUSTOM FUNCTIONS STARTS HERE */

/*
 * Overriding some parent theme functions
 */
include_once('include/functions_overrides.php');

/*
 * Setting functions for templates
 */
include_once('include/template-functions.php');

/*
 * Custom post types setup
 */
include_once('include/custom_posts_setup.php');

/*
 * Custom fields setup
 */
include_once('include/custom_fields_setup_acf5.php');

/**
 * Customizing login logo
 * https://codex.wordpress.org/Customizing_the_Login_Form
 */
 function panestates_login_logo() { ?>
     <style type="text/css">
         #login h1 a, .login h1 a {
             background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/logos/logo_2018_01_minimal_500x500.png);
     		height:80px;
     		width:80px;
     		background-size: 80px 80px;
     		background-repeat: no-repeat;
         }
     </style>
 <?php }
add_action( 'login_enqueue_scripts', 'panestates_login_logo' );

function panestates_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'panestates_login_logo_url' );

function panestates_login_logo_url_title() {
    return 'PAN Estates login';
}
add_filter( 'login_headertitle', 'panestates_login_logo_url_title' );

/*
 * Add admin css for custom fields layout
 * https://codex.wordpress.org/Creating_Admin_Themes
 * https://www.advancedcustomfields.com/resources/acf-input-admin_head/
 * https://www.advancedcustomfields.com/resources/acf-input-admin_enqueue_scripts/
 */

function admin_custom_fields_layout()
{
    wp_enqueue_style('admin-theme', get_stylesheet_directory_uri() . '/css/admin_layout_cf.css');
}
add_action('acf/input/admin_enqueue_scripts', 'admin_custom_fields_layout');

/**
 * Adding columns to admin properties page
 * https://gist.github.com/woraperth/a27ff834963eefbf45c5685d7096baea
 * https://catapultthemes.com/add-acf-fields-to-admin-columns/
 */
 // Add ACF Columns
function add_new_property_columns($columns) {
    $columns['property_id'] = 'ID';
    $columns['property_type'] = 'Type';
    $columns['price'] = 'Price';
    $columns['featured'] = 'Featured';

    return $columns;
}
add_filter('manage_property_posts_columns', 'add_new_property_columns');

function add_new_property_admin_column_show_value( $column, $post_id ) {
    if ($column == 'property_id') {
		$property_id = $post_id;
		echo $property_id;
	}
    if ($column == 'property_type') {
		$property_type = get_field_object('property_type');
		echo $property_type['choices'][$property_type['value']];
	}
    if ($column == 'price') {
		$property_price = get_field_object('price');
		echo number_format($property_price['value']) . ' ' . $property_price['append'];
	}
    if ($column == 'featured') {
		$property_featured = get_field_object('featured');
		echo ($property_featured['value'] ? 'Featured' : '');
	}
}
add_filter('manage_property_posts_custom_column', 'add_new_property_admin_column_show_value', 10, 2);

/* Make the column sortable */
// TODO: Sorting is not working properly...
// maybe https://wordpress.org/plugins/codepress-admin-columns/
function set_custom_property_sortable_columns( $columns ) {
    $columns['property_id'] = 'ID';
    $columns['property_type'] = 'Type';
    $columns['price'] = 'Price';
    $columns['featured'] = 'Featured';

  return $columns;
}
add_filter( 'manage_edit-property_sortable_columns', 'set_custom_property_sortable_columns' );

function property_custom_orderby( $query ) {
  if ( ! is_admin() )
    return;

  $orderby = $query->get('orderby');

  if ( 'property_id' == $orderby ) {
      $query->set( 'orderby', 'ID' );
  }
  if ( 'property_type' == $orderby ) {
    $query->set( 'meta_key', 'property_type' );
    $query->set( 'meta_type', 'CHAR' );
    $query->set( 'orderby', 'meta_value' );
  }
  if ( 'price' == $orderby ) {
    $query->set( 'meta_key', 'price' );
    $query->set( 'meta_type', 'NUMERIC' );
    $query->set( 'orderby', 'meta_value' );
  }
  if ( 'featured' == $orderby ) {
    $query->set( 'meta_key', 'featured' );
    $query->set( 'meta_type', 'BINARY' );
    $query->set( 'orderby', 'meta_value' );
  }
}
add_action( 'pre_get_posts', 'property_custom_orderby' );

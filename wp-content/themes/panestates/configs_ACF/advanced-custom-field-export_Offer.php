<?php
/*
== Instructions
Copy the PHP code generated
Paste into your functions.php file
To activate any Add-ons, edit and use the code in the first few lines.

== Notes
Registered field groups will not appear in the list of editable field groups. This is useful for including fields in themes.
Please note that if you export and register field groups within the same WP, you will see duplicate fields on your edit screens. To fix this, please move the original field group to the trash or remove the code from your functions.php file.

== Include in theme
The Advanced Custom Fields plugin can be included within a theme. To do so, move the ACF plugin inside your theme and add the following code to your functions.php file:
include_once('advanced-custom-fields/acf.php');

To remove all visual interfaces from the ACF plugin, you can use a constant to enable lite mode. Add the following code to your functions.php file before the include_once code:
define( 'ACF_LITE', true );
*/


// COPY FROM HERE

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_offer',
		'title' => '[:en]Offer[:bg]Offer[:]',
		'fields' => array (
			array (
				'key' => 'field_5b577f269cf72',
				'label' => 'Offer for',
				'name' => 'offer_type',
				'type' => 'select',
				'instructions' => 'Select offer type',
				'required' => 1,
				'choices' => array (
					'sale' => 'Sale',
					'rent' => 'Rent',
				),
				'default_value' => 'sale',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_5b578a1cc61a2',
				'label' => 'Property Type',
				'name' => 'property_type',
				'type' => 'select',
				'required' => 1,
				'choices' => array (
					'parcel' => 'Parcel',
					'apartment' => 'Apartment',
					'office' => 'Office',
					'house' => 'House',
					'villa' => 'Villa',
					'building' => 'Building',
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_5b578c7888c02',
				'label' => 'Price',
				'name' => 'price',
				'type' => 'number',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => 'BGN',
				'min' => '',
				'max' => '',
				'step' => '',
			),
			array (
				'key' => 'field_5b578d4fa22a6',
				'label' => 'Area',
				'name' => 'area',
				'type' => 'number',
				'instructions' => 'Area in square meters',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => 'm²',
				'min' => '',
				'max' => '',
				'step' => '',
			),
			array (
				'key' => 'field_5b578df117311',
				'label' => 'Floors',
				'name' => 'floors',
				'type' => 'number',
				'instructions' => 'Number of floors',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => 0,
				'max' => 100,
				'step' => 1,
			),
			array (
				'key' => 'field_5b578e748e1c4',
				'label' => 'Rooms',
				'name' => 'rooms',
				'type' => 'number',
				'instructions' => 'Number of rooms',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => 0,
				'max' => 100,
				'step' => 1,
			),
			array (
				'key' => 'field_5b578e9aa2d0a',
				'label' => 'Bathroom',
				'name' => 'bathrooms',
				'type' => 'number',
				'instructions' => 'Number of bathrooms',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => 0,
				'max' => 100,
				'step' => 1,
			),
			array (
				'key' => 'field_5b578ebe5e8ee',
				'label' => 'Neighbourhood',
				'name' => 'neighborhood',
				'type' => 'text',
				'instructions' => 'Common name of the nighborhood',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => 20,
			),
			array (
				'key' => 'field_5b578f4516bf8',
				'label' => 'Regulation',
				'name' => 'regulation',
				'type' => 'true_false',
				'instructions' => 'Status of regulation',
				'message' => 'Property is in regulation',
				'default_value' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'property',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'no_box',
			'hide_on_screen' => array (
				0 => 'permalink',
			),
		),
		'menu_order' => 0,
	));
}

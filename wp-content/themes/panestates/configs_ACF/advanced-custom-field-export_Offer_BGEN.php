<?php
/*
== Instructions
Copy the PHP code generated
Paste into your functions.php file
To activate any Add-ons, edit and use the code in the first few lines.

== Notes
Registered field groups will not appear in the list of editable field groups. This is useful for including fields in themes.
Please note that if you export and register field groups within the same WP, you will see duplicate fields on your edit screens. To fix this, please move the original field group to the trash or remove the code from your functions.php file.

== Include in theme
The Advanced Custom Fields plugin can be included within a theme. To do so, move the ACF plugin inside your theme and add the following code to your functions.php file:
include_once('advanced-custom-fields/acf.php');

To remove all visual interfaces from the ACF plugin, you can use a constant to enable lite mode. Add the following code to your functions.php file before the include_once code:
define( 'ACF_LITE', true );
*/


// COPY FROM HERE

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_offer',
		'title' => '[:en]Offer[:bg]Оферта[:]',
		'fields' => array (
			array (
				'key' => 'field_5b577f269cf72',
				'label' => '[:en]Offer for[:bg]Оферта за[:]',
				'name' => 'offer_type',
				'type' => 'select',
				'instructions' => '[:en]Select offer type[:bg]Изберете вид на офертата.[:]',
				'required' => 1,
				'choices' => array (
					'sale' => '[:en]Sale[:bg]Продажба[:]',
					'rent' => '[:en]Rent[:bg]Наем[:]',
				),
				'default_value' => 'sale',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_5b578a1cc61a2',
				'label' => '[:en]Property Type[:bg]Вид Имот[:]',
				'name' => 'property_type',
				'type' => 'select',
				'instructions' => '[:en]Select property type[:bg]Изберете вид на имота.[:]',
				'required' => 1,
				'choices' => array (
					'parcel' => '[:en]Parcel[:bg]Парцел[:]',
					'apartment' => '[:en]Apartment[:bg]Апартамент[:]',
					'office' => '[:en]Office[:bg]Офис[:]',
					'house' => '[:en]House[:bg]Къща[:]',
					'villa' => '[:en]Villa[:bg]Вила[:]',
					'building' => '[:en]Building[:bg]Сграда[:]',
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_5b578c7888c02',
				'label' => '[:en]Price[:bg]Цена[:]',
				'name' => 'price',
				'type' => 'number',
				'instructions' => '[:en]Offer price in bulgarian lev.[:bg]Оферта в лева.[:]',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '[:en]BGN[:bg]лв.[:]',
				'min' => '',
				'max' => '',
				'step' => '',
			),
			array (
				'key' => 'field_5b578d4fa22a6',
				'label' => '[:en]Area[:bg]Площ[:]',
				'name' => 'area',
				'type' => 'number',
				'instructions' => '[:en]Area in square meters[:bg]Площ в квадратни метри[:]',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '[:en]m²[:bg]м²[:]',
				'min' => '',
				'max' => '',
				'step' => '',
			),
			array (
				'key' => 'field_5b578df117311',
				'label' => '[:en]Floors[:bg]Етажи[:]',
				'name' => 'floors',
				'type' => 'number',
				'instructions' => '[:en]Number of floors[:bg]Брой на етажи[:]',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => 0,
				'max' => 100,
				'step' => 1,
			),
			array (
				'key' => 'field_5b578e748e1c4',
				'label' => '[:en]Rooms[:bg]Стаи[:]',
				'name' => 'rooms',
				'type' => 'number',
				'instructions' => '[:en]Number of rooms[:bg]Брой на стаи[:]',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => 0,
				'max' => 100,
				'step' => 1,
			),
			array (
				'key' => 'field_5b578e9aa2d0a',
				'label' => '[:en]Bathrooms[:bg]бани[:]',
				'name' => 'bathrooms',
				'type' => 'number',
				'instructions' => '[:en]Number of bathrooms[:bg]Брой на бани[:]',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => 0,
				'max' => 100,
				'step' => 1,
			),
			array (
				'key' => 'field_5b578ebe5e8ee',
				'label' => '[:en]Neighbourhood[:bg]Квартал / Район[:]',
				'name' => 'neighborhood',
				'type' => 'text',
				'instructions' => '[:en]Common name of the neighborhood[:bg]Име на квартала / район[:]',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => 20,
			),
			array (
				'key' => 'field_5b578f4516bf8',
				'label' => '[:en]Regulation[:bg]Регулация[:]',
				'name' => 'regulation',
				'type' => 'true_false',
				'instructions' => '[:en]Regulation status of the real estate[:bg]Статут на регулация на имота[:]',
				'message' => '[:en]Property is in regulation[:bg]Имот е в регулация[:]',
				'default_value' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'property',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'no_box',
			'hide_on_screen' => array (
				0 => 'permalink',
			),
		),
		'menu_order' => 0,
	));
}

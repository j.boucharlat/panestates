<?php
/*
== Instructions
Copy the PHP code generated
Paste into your functions.php file
To activate any Add-ons, edit and use the code in the first few lines.

== Notes
Registered field groups will not appear in the list of editable field groups. This is useful for including fields in themes.
Please note that if you export and register field groups within the same WP, you will see duplicate fields on your edit screens. To fix this, please move the original field group to the trash or remove the code from your functions.php file.

== Include in theme
The Advanced Custom Fields plugin can be included within a theme. To do so, move the ACF plugin inside your theme and add the following code to your functions.php file:
include_once('advanced-custom-fields/acf.php');

To remove all visual interfaces from the ACF plugin, you can use a constant to enable lite mode. Add the following code to your functions.php file before the include_once code:
define( 'ACF_LITE', true );
*/


// COPY FROM HERE

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_pictures',
		'title' => '[:en]Pictures[:bg]Снимки[:]',
		'fields' => array (
			array (
				'key' => 'field_5b757919becea',
				'label' => 'Featured picture',
				'name' => 'pictures_featured',
				'type' => 'image',
				'instructions' => 'This is the picture that will show up in listings, sidebars...',
				'required' => 1,
				'save_format' => 'object',
				'preview_size' => 'medium',
				'library' => 'uploadedTo',
			),
			array (
				'key' => 'field_5b57ae4dac665',
				'label' => 'Pictures of the property',
				'name' => 'pictures_gallery',
				'type' => 'photo_gallery',
				'instructions' => 'These pictures will be displayed as a slideshow in the listing.',
				'edit_modal' => 'Default',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'property',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 1,
	));
}

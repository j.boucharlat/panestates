<?php
/**
 * Template Name: Full Width Sales
 * @package PAN_Estates
 */

get_header();
?>

    <?php do_action('hotel_luxury_page_before_content'); ?>

	<div id="primary" class="content-area row">
		<div class="content-wrapper col-md-12">
			<main id="main" class="site-main row">

				<?php
                $args = array(
                    'posts_per_page'	=> -1,
                    'post_type'			=> 'property',
                    'meta_key'		=> 'offer_type',
                    'meta_value'	=> 'sale',
                    'order' => 'DESC',
                    'orderby' => 'date',
                );

                // https://codex.wordpress.org/The_Loop#Multiple_Loops
                query_posts($args);

                if (have_posts()) : ?>

        			<?php
                    /* Start the Loop */
                    while (have_posts()) : the_post();

                        /*
                         * Include the Post-Format-specific template for the content.
                         * If you want to override this in a child theme, then include a file
                         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                         */
                        get_template_part('template-parts/content', 'list-property');

                    endwhile;

                    the_posts_navigation();

                else :

                    get_template_part('template-parts/content', 'none');

                endif;				?>

			</main><!-- #main -->
		</div>

	</div><!-- #primary -->

<?php
get_footer();

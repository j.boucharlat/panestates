<?php
/**
 * Template part for displaying post list Layout.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package PAN_Estates
 */

/*
 *  Getting custom fields values & setting some vars
 *  https://www.advancedcustomfields.com/resources/get_field_objects/
 */
 $details = get_field_objects();
 // echo "<pre>";
 // print_r($details);
 // echo "</pre></br></br>";
 // // TODO: use "case" for each attribute icon / style
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('offer-card col-lg-6 col-md-6 col-sm-6 col-xs-12'); ?>>

	<div class="offer-holder">
		<div class="offer-image-holder">
			<a href="<?php the_permalink() ?>" style="display: block" title="<?php the_title_attribute(); ?>">
				<?php if (has_post_thumbnail()) {
                        the_post_thumbnail('large');
                    } else { ?>
                        <img src="https://via.placeholder.com/600x600" class="attachment-large size-large wp-post-image" alt=""><?php
                    } ?>
				<div class="offer-price-holder">
					<span class="offer-price">
						<?php echo number_format($details['price']['value']); ?>
					</span>
					<span class="offer-price offer-append">
						<?php echo $details['price']['append']; ?>
					</span>
                    <span class="right">
                        <?php echo $details['offer_type']['choices'][$details['offer_type']['value']]; ?>
                    </span>
				</div>
			</a>
		</div>

		<div class="offer-description">
			<a href="<?php the_permalink() ?>">
				<h1 class="offer-title"><?php the_title_attribute(); ?><span class="ref-number right">Ref. ID: <?php the_ID(); ?></span></h1>
			</a>

			<ul class="offer-details row">
                <!-- Estate type -->
				<li class="col-md-6 left">
					<span class="icon-holder">
						<i class="panicon panicon-type"></i>
					</span>
					<?php echo $details['property_type']['choices'][$details['property_type']['value']]; ?>
				</li>

				<!-- Location -->
                <?php if ( $details['neighborhood']['value'] != '' ) { ?>
    				<li class="col-md-6 left">
    					<span class="icon-holder">
    						<i class="panicon panicon-location"></i>
    					</span>
    					<?php echo $details['neighborhood']['value']; ?>
    				</li>
                <?php }  ?>

				<!-- Area -->
				<li class="col-md-6 left">
					<span class="icon-holder">
						<i class="panicon panicon-area"></i>
					</span>
					<?php echo $details['area']['value'] . ' ' . $details['area']['append']; ?>
				</li>

				<!-- Plot -->

				<!-- Floors -->
                <?php if ( $details['floors']['value'] > 0 ) { ?>
				<li class="col-md-6 left">
					<span class="icon-holder">
						<i class="panicon panicon-floors"></i>
					</span>
					<?php echo $details['floors']['value'] . ' ' . $details['floors']['label']; ?>
				</li>
                <?php }  ?>

				<!-- Rooms -->
                <?php if ( $details['rooms']['value'] > 0 || $details['bathrooms']['value'] > 0) { ?>
    				<li class="col-md-6 left">
    					<span class="icon-holder">
    						<i class="panicon panicon-rooms"></i>
    					</span>
                        <?php
                        echo $details['rooms']['value'] > 0 ? $details['rooms']['value'] . ' ' . $details['rooms']['label'] . ($details['bathrooms']['value'] > 0 ? ', ' : '') : '';
                        echo $details['bathrooms']['value'] > 0 ? $details['bathrooms']['value'] . ' ' . $details['bathrooms']['label'] : '';
                        ?>
    				</li>
                <?php }  ?>

				<!-- Regulation -->
                <?php if ( $details['property_type']['value'] == 'parcel') { ?>
					<li class="col-md-6 left">
						<span class="icon-holder">
							<i class="panicon panicon-legal"></i>
						</span>
						<?php echo $details['regulation']['label'] . ' : ' . $details['regulation']['value'] = (true) ? __('[:en]Yes[:bg]Да[:]') : __('[:en]No[:bg]Не[:]') ; ?>
					</li>
				<?php }  ?>
			</ul>
		</div>
	</div>

</article><!-- #post-## -->

<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package PAN_Estates
 */

/*
 *  Getting custom fields
 *  https://www.advancedcustomfields.com/resources/get_field_objects/
 */
// $fields = get_field_objects();
$details = get_field_objects();
// echo "<pre>";
// print_r($details);
// echo "</pre></br></br>";
// // TODO: use "case" for each attribute icon / style


?>

<article id="post-<?php the_ID(); ?>" <?php post_class('row property-single'); ?>>

    <div class="page-title-wrapper col-md-12">
        <h1 class="page-title">
            <?php single_post_title(); ?>
            <span class="offer-price-append right">
                <?php echo ' ' . $details['price']['append']; ?>
            </span>
            <span class="offer-price right">
                <?php echo number_format($details['price']['value']) . ' '; ?>
            </span>
        </h1>
        <span class="ref-number right">
            Ref. ID: <?php the_ID(); ?>
        </span>
        <span class="property-type left">
            <?php echo $details['offer_type']['choices'][$details['offer_type']['value']]; ?>
        </span>
        <!-- <div class="clear"></div> -->
    </div>

	<div class="property-galllery col-md-12">
		<?php echo do_shortcode('[acf_gallery_slider acf_field="pictures_gallery" filmstrip="true"]'); ?>
	</div>


	<div class="property-details col-md-4">
        <ul class="offer-details row">
            <!-- Estate type -->
            <li class="col-md-12 left">
                <span class="icon-holder">
                    <i class="panicon panicon-type"></i>
                </span>
                <?php echo $details['property_type']['choices'][$details['property_type']['value']]; ?>
            </li>

            <!-- Location -->
            <?php if ( $details['neighborhood']['value'] != '' ) { ?>
                <li class="col-md-12 left">
                    <span class="icon-holder">
                        <i class="panicon panicon-location"></i>
                    </span>
                    <?php echo $details['neighborhood']['value']; ?>
                </li>
            <?php }  ?>

            <!-- Area -->
            <li class="col-md-12 left">
                <span class="icon-holder">
                    <i class="panicon panicon-area"></i>
                </span>
                <?php echo $details['area']['value'] . ' ' . $details['area']['append']; ?>
            </li>

            <!-- Plot -->

            <!-- Floors -->
            <?php if ( $details['floors']['value'] > 0 ) { ?>
                <li class="col-md-12 left">
                    <span class="icon-holder">
                        <i class="panicon panicon-floors"></i>
                    </span>
                    <?php echo $details['floors']['value'] . ' ' . $details['floors']['label']; ?>
                </li>
            <?php }  ?>

            <!-- Rooms -->
            <?php if ( $details['rooms']['value'] > 0 || $details['bathrooms']['value'] > 0) { ?>
                <li class="col-md-12 left">
                    <span class="icon-holder">
                        <i class="panicon panicon-rooms"></i>
                    </span>
                    <?php
                    echo $details['rooms']['value'] > 0 ? $details['rooms']['value'] . ' ' . $details['rooms']['label'] . ($details['bathrooms']['value'] > 0 ? ', ' : '') : '';
                    echo $details['bathrooms']['value'] > 0 ? $details['bathrooms']['value'] . ' ' . $details['bathrooms']['label'] : '';
                    ?>
                </li>
            <?php }  ?>

            <!-- Regulation -->
            <?php if ( $details['property_type']['value'] == 'parcel') { ?>
                <li class="col-md-12 left">
                    <span class="icon-holder">
                        <i class="panicon panicon-legal"></i>
                    </span>
                    <?php echo $details['regulation']['label'] . ' : ' . $details['regulation']['value'] = (true) ? __('[:en]Yes[:bg]Да[:]') : __('[:en]No[:bg]Не[:]') ; ?>
                </li>
            <?php }  ?>
        </ul>
	</div>

    <div class="text-content col-md-8">
        <?php the_content() ; ?>
    </div>

    <div class="post-heading col-md-12">
    </div>
    <div class="entry-footer col-md-12">
		<?php hotel_luxury_entry_footer() ; ?>
        <div class="blog-meta">
            <?php hotel_luxury_posted_on(); ?>
        </div>
    </div>

</article><!-- #post-<?php the_ID(); ?> -->

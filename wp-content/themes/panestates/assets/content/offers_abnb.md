## #1 [Great located architect's penthouse apartment](https://www.airbnb.com/rooms/6581883?location=Bulgaria&home_collection=1&s=9Be6S-oO)  

#### Plovdiv  

- 6 guests  
- 3 bedrooms  
- 6 beds  
- 1.5 baths  

![](https://a0.muscache.com/im/pictures/82876519/4afec91f_original.jpg?aki_policy=xx_large)  
![](https://a0.muscache.com/im/pictures/82876490/a9ebaad7_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/82876521/29604a41_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/82876496/812cb3c4_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/82876500/8fd29b3a_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/82876502/1f1547ce_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/82877489/d7326bc3_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/82876989/fd260263_original.jpg?aki_policy=x_large)  

## #2 [Brand new boutique house close to the Residency](https://www.airbnb.com/rooms/22974763?location=Bulgaria&home_collection=1&s=9Be6S-oO)  

#### Sofia  
- 10 guests  
- 4 bedrooms  
- 7 beds  
- 3 baths  

![](https://a0.muscache.com/im/pictures/a6a1024e-e83f-4a7d-a8f8-ee2de30cece9.jpg?aki_policy=xx_large)  
![](https://a0.muscache.com/im/pictures/ed26aa4f-e700-4888-b1fc-d178ced3560d.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/f58420b2-8e65-49f1-9216-f4e43ddee958.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/27107764-a287-409f-8a34-b730fbc0e87a.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/dfaf0513-b80a-49d8-862d-1713f5028f42.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/92f4ce1e-a495-444f-9d76-bc3d741bb6df.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/71270e92-82eb-44a8-aaa3-e2d33d8b62fa.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/1809bbb6-a6be-4d53-9842-081bdaf329c4.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/8d6aa41b-bf83-4960-bf10-885d84cf44f1.jpg?aki_policy=x_large)  

## #3 [Cubehouse in central Rotterdam](https://www.airbnb.co.uk/rooms/817858)  

### Rotterdam  
- 4 guests  
- 2 bedrooms  
- 3 beds  
- 1 bath  

![](https://a0.muscache.com/im/pictures/16536733/dceb46ba_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/16534619/064511b7_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/16536552/eb5dd083_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/16536533/a6ae41e7_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/16535328/e4371473_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/16536515/f1c843ea_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/3deb454c-a7aa-4d3c-a8b4-8fbeee376439.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/a8aafea2-2ddf-4873-9e2e-7932b37e2a6d.jpg?aki_policy=x_large)  

## #4 [Amazing view - Moderne apartment](https://www.airbnb.co.uk/rooms/1083329)  

### Akureyri  
- 4 guests  
- 2 bedrooms  
- 3 beds  
- 1 bath  

![](https://a0.muscache.com/im/pictures/25625163/d4833a1c_original.jpg?aki_policy=xx_large)  
![](https://a0.muscache.com/im/pictures/25625125/9d06ac17_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/97a4b034-6bdc-43ea-b2cc-0bcbbbf211b1.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/e2a4288f-d5ab-44a0-af3e-55d424918389.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/4bfd386a-0b8a-4ebf-8814-8cead9e20fb0.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/d5925c0d-933b-4e68-9f72-0ee7995776cb.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/24164e67-fc2c-4db0-901b-57d6d033d6fa.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/b010f8ef-8e77-46f3-b1db-bfd759580f23.jpg?aki_policy=x_large)  

## #5 [Romantic STUDIO](https://www.airbnb.co.uk/rooms/6081231?s=aINO3NY6)  

### Prague  
- 2 guests  
- Studio  
- 2 beds  
- 1 bath  

![](https://a0.muscache.com/im/pictures/84946732/b836e2ef_original.jpg?aki_policy=xx_large)  
![](https://a0.muscache.com/im/pictures/86511476/c4b38280_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/100586375/ea33cc91_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/84946686/015538db_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/100586254/c6e9c376_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/84946672/25c1d64c_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/84946617/5dbdf12f_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/84392791/5df5f070_original.jpg?aki_policy=x_large)  

## #6 [Boutique apt with AC - CENTRAL AREA - 100m2](https://www.airbnb.co.uk/rooms/2218976?s=aINO3NY6)  

### Prague  
- 2 guests  
- 1 bedroom  
- 1 bed  
- 1.5 baths  

![](https://a0.muscache.com/im/pictures/c3daac7d-14a3-448c-bd9e-5ac6b5144308.jpg?aki_policy=xx_large)  
![](https://a0.muscache.com/im/pictures/32190289/a54b4555_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/32191097/3acec51d_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/32190896/0095392f_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/a897133b-ba96-49d6-bf8c-1ca51d54c0e0.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/611a49f3-4a67-4955-9a4f-68dc454a5a56.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/1fa42245-09f5-47d4-ba5b-d36569d5ca2c.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/32191213/cf10c5a7_original.jpg?aki_policy=x_large)  

## #7 [Loft Studio in the Central Area](https://www.airbnb.co.uk/rooms/457458?s=aINO3NY6)  

### Moscow  
- 3 guests  
- Studio  
- 2 beds  
- 1 bath  

![](https://a0.muscache.com/im/pictures/6152848/b04eddeb_original.jpg?aki_policy=xx_large)  
![](https://a0.muscache.com/im/pictures/6152640/3e950e66_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/6152672/cccc1591_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/6256408/6c58f29a_original.jpg?aki_policy=x_large)  
![](https://a0.muscache.com/im/pictures/6153028/0e0920bc_original.jpg?aki_policy=x_large)  


## Interesting ones  
[GEODESIC DOME AND COSY SPACE IN NATURE](https://www.airbnb.co.uk/rooms/621940)  
[Koe in de Kost](https://www.airbnb.co.uk/rooms/930940)  
[THE FISHERMANS' CAVE HOUSE](https://www.airbnb.co.uk/rooms/388119)  
[Old Smock Windmill in rural Kent](https://www.airbnb.co.uk/rooms/1237965)  
[27 Incredible Airbnb Locations In Europe](https://www.buzzfeed.com/danieldalton/amazing-airbnb-locations-in-europe?utm_term=.buxwxKDE0#.mo8mMO2kx)  

# Notes for wpm-config configuration

### admin_pages = admin screens to configure

Screen ID can be found :  
-   in Query Monitor summary
-   by searching for `pagenow` or `adminpage` value with DevTools  
    `adminpage = 'settings_page_cookie-notice',`
-   checking `<body>` class
-   checking `get_current_screen()` (PHP function) result

### Targetting admin options

##### On single page options

1.  target the form first : search for `name="option_page"`'s `value`  
    i.e. in `<input type="hidden" name="option_page" value="cookie_notice_options">` will be `cookie_notice_options`
2.  target the inputs inside the form by searching for form's `value`  
    i.e. in `<textarea name="cookie_notice_options[message_text]" class="large-text" cols="50" rows="5">` will be `message_text`

Which will produce the following `wpm-config.json` :  

```json
{
    "admin_pages": [
        "settings_page_cookie-notice",
    ],
    "options": {
        "cookie_notice_options": {
            "message_text": {},
            "accept_text": {},
            "refuse_text": {}
        }
    }
}
```
##### On tabbed pages options  
1. find the field you want to enable translation to (DevTools)
1. parameters to put in `options` array will be found in input's name :  
`<input type="text" name="ctcc_content_settings[heading_text]" value="Cookies BG">`
1. build config accordingly :
```json
...
"options": {
    "ctcc_content_settings": {
        "heading_text": {}
    }
}
...
```

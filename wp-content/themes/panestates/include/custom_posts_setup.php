<?php 
/*
 * Property custom post type setup
 * Author: Julien
 *
 * http://blog.teamtreehouse.com/adding-custom-fields-to-a-custom-post-type-the-right-way
 * https://www.freshconsulting.com/wordpress-custom-post-types-advanced-custom-fields/
 * https://pressable.com/blog/2016/09/08/advanced-custom-fields-custom-post-types/
 *
 * Done with https://generatewp.com/post-type/?clone=Vp5bRnA
*/

if ( ! function_exists('property_post_type') ) {

// Register Custom Post Type
function property_post_type() {

	$labels = array(
		'name'                  => _x( 'Properties', 'Post Type General Name', 'panestates' ),
		'singular_name'         => _x( 'Property', 'Post Type Singular Name', 'panestates' ),
		'menu_name'             => __( 'Properties', 'panestates' ),
		'name_admin_bar'        => __( 'Property', 'panestates' ),
		'archives'              => __( 'Property Archives', 'panestates' ),
		'attributes'            => __( 'Property Attributes', 'panestates' ),
		'parent_item_colon'     => __( 'Parent Property', 'panestates' ),
		'all_items'             => __( 'All Properties', 'panestates' ),
		'add_new_item'          => __( 'Add New Property', 'panestates' ),
		'add_new'               => __( 'Add New Property', 'panestates' ),
		'new_item'              => __( 'New Property', 'panestates' ),
		'edit_item'             => __( 'Edit Property', 'panestates' ),
		'update_item'           => __( 'Update Property', 'panestates' ),
		'view_item'             => __( 'View Property', 'panestates' ),
		'view_items'            => __( 'View Properties', 'panestates' ),
		'search_items'          => __( 'Search Property', 'panestates' ),
		'not_found'             => __( 'No properties found', 'panestates' ),
		'not_found_in_trash'    => __( 'No properties in Trash', 'panestates' ),
		'featured_image'        => __( 'Featured Image', 'panestates' ),
		'set_featured_image'    => __( 'Set featured image', 'panestates' ),
		'remove_featured_image' => __( 'Remove featured image', 'panestates' ),
		'use_featured_image'    => __( 'Use as featured image', 'panestates' ),
		'insert_into_item'      => __( 'Insert into property', 'panestates' ),
		'uploaded_to_this_item' => __( 'Uploaded to this property', 'panestates' ),
		'items_list'            => __( 'Property listing', 'panestates' ),
		'items_list_navigation' => __( 'Property listing navigation', 'panestates' ),
		'filter_items_list'     => __( 'Filter property listing', 'panestates' ),
	);
	$args = array(
		'label'                 => __( 'Property', 'panestates' ),
		'description'           => __( 'Real estate property offer.', 'panestates' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
		'taxonomies'            => array( 'region', 'city' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 25,
		'menu_icon'             => 'dashicons-admin-home',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'property', $args );

}
add_action( 'init', 'property_post_type', 0 );

}

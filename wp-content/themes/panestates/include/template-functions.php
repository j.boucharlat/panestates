<?php
// Exit if accessed directly
if (!defined('ABSPATH')) {
 exit;
}



/**
 * Forcing to display header on call from template
 *
 */
add_action('panestates_force_title_bar', 'panestates_display_title_bar');
function panestates_display_title_bar() {

    // add_filter( 'body_class', 'force_body_class', 20);
    // function force_body_class() {
    //     str_replace('no-titlebar','has-titlebar',$classes);
    // }

    $header_image = get_header_image();
    if ( ! empty($header_image) ) {
        $css = 'style="background-image: url('. $header_image .')"';
    } else {
        $css = '';
    }
    ?>
    <div class="titlebar-outer-wrapper" <?php echo $css; ?> >

        <div class="titlebar-title">
                    <h2><?php echo get_the_title( $page_id ) ?></h2>
                    <div class="breadcrumbs">
                        <?php
                        if(function_exists('bcn_display' ) )
                        {
                            bcn_display();
                        }
                        ?>
                    </div>
        </div>

    </div>
    <?php
}


/**
* Featured properties row
* @var [type]
*/
add_action('panestates_front_before_content', 'panestates_featured_row');

function panestates_featured_row()
{
    // echo "<pre>in tpl functions!!</pre>";
    // Filtering featured properties
     $args = array(
         'posts_per_page'	=> 3,
         'post_type'			=> 'property',
         'meta_key'		=> 'featured',
         'meta_value'	=> 1,
         'order' => 'DESC',
         'orderby' => 'date',
     );

    // $properties = new WP_Query( $args );
    // echo "<pre>";
    // // print_r($properties);
    // echo "</pre></br></br>";
    $properties = get_posts($args);
    // echo "<pre>";
    // print_r($properties_posts);
    // echo "</pre></br></br>";

    if ($properties) {
         ?>
         <div class="featured-properties-row row">
         <?php
        // while ( $properties->have_posts() ): $properties->the_post();
        foreach ($properties as $property):

            $thumbnail_url = wp_get_attachment_image_src(get_post_thumbnail_id($property->ID), 'large', true);
         $property_id = $property->ID;
         // echo "<pre>";
         // print_r($property);
         // echo "</pre></br></br>";
         echo "<pre>";
         print_r($thumbnail_url);
         echo "</pre></br></br>"; ?>
             <div class="offer-card col-md-4">
                 <div class="offer-holder">
                     <div class="offer-image-holder">
                         <a href="<?php echo get_the_permalink($property_id) ?>" style="display: block" ?>">
                         <?php if ($thumbnail_url) { ?>
                             <img src="<?php echo esc_url($thumbnail_url[0]) ?>" alt="" />
                         <?php } else { ?>
                             <img src="https://via.placeholder.com/600x600" class="attachment-large size-large wp-post-image" alt="">
                             <?php } ?>
                             <div class="offer-price-holder">
                                 <span class="offer-price">
                                     <?php echo get_the_title($property_id); ?>
                                 </span>
                             </div>
                         </a>
                     </div>
                 </div>
             </div>
     		<?php endforeach; ?>
    </div>
    <?php
    }
    wp_reset_postdata();
 }

 /**
  * Last properties row
  * @var [type]
  */
 add_action('panestates_front_after_content', 'panestates_lasts_row');

 function panestates_lasts_row()
 {
     // echo "<pre>in tpl functions!!</pre>";

     $args = array(
         'post_type' => 'property',
         'order' => 'DESC',
         'orderby' => 'date',
         'posts_per_page' => 3,
     );

     // $properties = new WP_Query( $args );
     // echo "<pre>";
     // // print_r($properties);
     // echo "</pre></br></br>";
     $properties = get_posts($args);
     // echo "<pre>";
     // print_r($properties_posts);
     // echo "</pre></br></br>";

     if ($properties) {
         ?>
         <div class="builder-title-wrapper clearfix has_filter">
			<h3 class="builder-item-title"><?php
            $string = '[:en]Last offers[:bg]Последни оферти[:]';
            $string = wpm_translate_string( $string );
            echo $string;
             ?></h3>
		</div>
         <div class="last-properties-row room-items row clearfix"><?php // TODO: fix room-items ?>
         <?php
        // while ( $properties->have_posts() ): $properties->the_post();
        foreach ($properties as $property):

            $thumbnail_url = wp_get_attachment_image_src(get_post_thumbnail_id($property->ID), 'large', true);
         $property_id = $property->ID;
         $property_price = get_field_object('price', $property_id);
         // echo "<pre>";
         // print_r($property_price);
         // echo "</pre></br></br>"; ?>
            <div class="mix col-md-4 column">
                <div class="hover offer-image-holder">
                    <?php if ($thumbnail_url) {
                    ?>
                        <img width="642" height="335" src="<?php echo esc_url($thumbnail_url[0]) ?>" class="attachment-hotel_luxury_medium size-hotel_luxury_medium wp-post-image" alt="">
                    <?php } else { ?>
                        <img width="642" height="335" src="https://via.placeholder.com/600x600" class="attachment-hotel_luxury_medium size-hotel_luxury_medium wp-post-image" alt="">
                    <?php } ?>

                    <div class="overlay">
                        <a href="<?php echo get_the_permalink($property_id) ?>" class="info"><span><i class="fa fa-link"></i></span></a>
                    </div>
                </div>
				<div class="cpt-detail clearfix">
					<h2 class="cpt-title">
						<a href="<?php echo get_the_permalink($property_id) ?>">
                            <?php echo get_the_title($property_id); ?>
                        </a>
					</h2>
					<div class="cpt-desc">
                        <p><?php echo number_format($property_price['value']) . ' ' . $property_price['append'] ?></p>
                    </div>
				</div>
			</div>
     		<?php endforeach; ?>
     </div>
     <?php
     }
     wp_reset_postdata();
 }


 /**
  * Footer Copyright
  */
 function panestates_footer_info() {
 	echo sprintf( esc_html__( 'Copyright &copy; %1$s %2$s', 'hotel-luxury' ), date_i18n( __('Y', 'panestates') ), '<a href="'.esc_url( home_url( '/' ) ).'" title="'.esc_attr( get_bloginfo( 'name', 'display' ) ).'">'.esc_html( get_bloginfo( 'name', 'display' ) ).'</a>');
 }
 add_action( 'panestates_footer_copyright', 'panestates_footer_info' );

<?php
/**
 * Template Name: Front Page
 * @package PAN_Estates
 */

get_header();
?>

    <?php do_action('hotel_luxury_page_before_content'); ?>
    <?php do_action('panestates_front_before_content'); ?>

	<div id="primary" class="content-area row">
		<div class="content-wrapper col-md-12">
			<main id="main" class="site-main">

				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content', 'page' );

				endwhile; // End of the loop.
				?>

			</main><!-- #main -->
		</div>

	</div><!-- #primary -->

    <?php do_action('panestates_front_after_content'); ?>

<?php
$mods = get_theme_mods();
// echo "<pre>";
// print_r($mods);
// echo "</pre></br></br>";

get_footer();
